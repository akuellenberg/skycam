#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
import cv2
import argparse
import zwoasi as asi
import os
import sys
from datetime import datetime
from pprint import pprint

# import required library like Gstreamer and GstreamerRtspServer
gi.require_version('Gst', '1.0')
gi.require_version('GstRtspServer', '1.0')
from gi.repository import Gst, GstRtspServer, GLib


class Cam():
    def __init__(self):
        env_filename = os.getenv('ZWO_ASI_LIB')
        if env_filename:
            asi.init(env_filename)
        else:
            print('Set ZWO_ASI_LIB...')
            sys.exit(1)
        num_cams = asi.get_num_cameras()
        if (num_cams == 0):
            print('No cam')
            sys.exit(1)

        cams_found = asi.list_cameras()
        cam_id = 0
        self.cam = asi.Camera(cam_id)
        self.info = self.cam.get_camera_property()
        self.controls = self.cam.get_controls()
        pprint(self.info)
        print("\n")
        pprint(self.controls)
        self.cam.start_video_capture()
        self.cam.set_image_type(asi.ASI_IMG_RAW16)
        self.cam.set_control_value(asi.ASI_BANDWIDTHOVERLOAD, self.cam.get_controls()['BandWidth']['MinValue'])

        self.cam.set_control_value(asi.ASI_EXPOSURE, self.controls['Exposure']['DefaultValue'], auto=True);
        self.cam.set_control_value(asi.ASI_GAIN, self.controls['Gain']['DefaultValue'], auto=True);

        self.cam.disable_dark_subtract()

        #self.cam.set_control_value(asi.ASI_GAIN, 50)
        #self.cam.set_control_value(asi.ASI_EXPOSURE, 10000)
        self.cam.set_control_value(asi.ASI_WB_B, 99)
        self.cam.set_control_value(asi.ASI_WB_R, 75)
        self.cam.set_control_value(asi.ASI_GAMMA, 50)
        self.cam.set_control_value(asi.ASI_BRIGHTNESS, 50)
        self.cam.set_control_value(asi.ASI_FLIP, 0)

    def get_img(self):
        img = self.cam.capture_video_frame()
        return img

    def get_df(self):
        df = self.cam.get_dropped_frames()
        print(f"df: {df}\n")



# Sensor Factory class which inherits the GstRtspServer base class and add
# properties to it.
class SensorFactory(GstRtspServer.RTSPMediaFactory):
    def __init__(self, **properties):
        super(SensorFactory, self).__init__(**properties)
        #self.img = cv2.imread('test.jpg')
        self.cam = Cam()
        self.number_frames = 0
        self.fps = opt.fps
        self.duration = 1 / self.fps * Gst.SECOND  # duration of a frame in nanoseconds
        self.launch_string = 'appsrc name=source is-live=true block=true format=GST_FORMAT_TIME ' \
                             'caps=video/x-raw,format=GRAY16_LE,width={},height={},framerate={}/1 ' \
                             '! videoconvert ! video/x-raw,format=I420 ' \
                             '! x264enc speed-preset=ultrafast tune=zerolatency ' \
                             '! rtph264pay config-interval=1 name=pay0 pt=96' \
                             .format(opt.image_width, opt.image_height, self.fps)
    # method to capture the video feed from the camera and push it to the
    # streaming buffer.
    def on_need_data(self, src, length):
        frame = self.cam.get_img()
        self.cam.get_df()
        # It is better to change the resolution of the camera 
        # instead of changing the image shape as it affects the image quality.
        frame = cv2.resize(frame, (opt.image_width, opt.image_height), \
            interpolation = cv2.INTER_LINEAR)

        font = cv2.FONT_HERSHEY_PLAIN
        org = (50,50)
        fontScale = 3
        color = 2**16-1
        thickness = 2 
        frame = cv2.putText(frame, datetime.now().strftime("%d.%m.%Y %H:%M:%S") , org, font, fontScale, color, thickness, cv2.LINE_AA)

        data = frame.tobytes()
        buf = Gst.Buffer.new_allocate(None, len(data), None)
        buf.fill(0, data)
        buf.duration = self.duration
        timestamp = self.number_frames * self.duration
        buf.pts = buf.dts = int(timestamp)
        buf.offset = timestamp
        self.number_frames += 1
        retval = src.emit('push-buffer', buf)
        print('pushed buffer, frame {}, duration {} ns, durations {} s'.format(self.number_frames,
                                                                               self.duration,
                                                                               self.duration / Gst.SECOND))
        if retval != Gst.FlowReturn.OK:
            print(retval)

    # attach the launch string to the override method
    def do_create_element(self, url):
        return Gst.parse_launch(self.launch_string)
    
    # attaching the source element to the rtsp media
    def do_configure(self, rtsp_media):
        self.number_frames = 0
        appsrc = rtsp_media.get_element().get_child_by_name('source')
        appsrc.connect('need-data', self.on_need_data)

# Rtsp server implementation where we attach the factory sensor with the stream uri
class GstServer(GstRtspServer.RTSPServer):
    def __init__(self, **properties):
        super(GstServer, self).__init__(**properties)
        self.factory = SensorFactory()
        self.factory.set_shared(True)
        self.get_mount_points().add_factory(opt.stream_uri, self.factory)
        self.attach(None)

# getting the required information from the user 
parser = argparse.ArgumentParser()
parser.add_argument("--fps", required=True, help="fps of the camera", type = int)
parser.add_argument("--image_width", required=True, help="video frame width", type = int)
parser.add_argument("--image_height", required=True, help="video frame height", type = int)
parser.add_argument("--stream_uri", default = "/video_stream", help="rtsp video stream uri")
opt = parser.parse_args()

# initializing the threads and running the stream on loop.
#GLib.threads_init()
Gst.init(None)
server = GstServer()
loop = GLib.MainLoop()
loop.run()
