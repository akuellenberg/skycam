docker images
docker build -t mpifr/asi_rtsp_server:0.1 .
docker rmi -f $(docker images -q --filter "dangling=true")

docker run -p 8554:8554 --privileged --rm -ti mpifr/asi_rtsp_server:0.1 /bin/bash
export ZWO_ASI_LIB=/usr/local/lib/libASICamera2.so 
./asi_rtsp_server.py --fps 1 --image_width 800 --image_height 600

vlc rtsp://134.104.73.21:8554/video_stream

