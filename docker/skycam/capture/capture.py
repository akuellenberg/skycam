#!/usr/bin/env python3
import os
import io
import sys
import time
import signal
import logging
import socket
import argparse
import threading
import numpy as np
import zwoasi as asi
from PIL import Image, ImageFont, ImageDraw, ImageEnhance
from waitress import create_server
from flask import Flask, send_file, request, jsonify
from flask_cors import CORS

FONT = "/usr/share/fonts/truetype/freefont/DejaVuSans.ttf"
IMAGE_SIZE = (3096,2080)
MAX_ERROR = 5
DEFAULT_PORT = 5000
DEFAULT_GAIN = 0 #100
DEFAULT_EXPOSURE = 1000 #30000
DEFAULT_GAMMA = 50
DEFAULT_BRIGHTNESS = 50
TIMEOUT = 20

app = Flask(__name__)
CORS(app)

class Skycam():
    lock = threading.Lock()
    gain = DEFAULT_GAIN
    exposure = DEFAULT_EXPOSURE
    gamma = DEFAULT_GAMMA
    brightness = DEFAULT_BRIGHTNESS
    error_count = 0

    def __init__(self, sdk):        
        logging.info(f'Using SDK: {sdk}') 
        # Initialize zwoasi with the name of the SDK library
        try:
            asi.init(sdk)
        except asi.ZWO_Error:
            raise RuntimeError('Could not initialize ZWO library')

        num_cameras = asi.get_num_cameras()
        if num_cameras == 0:
            raise RuntimeError('No camera found')

        cameras_found = asi.list_cameras()  # Models names of the connected cameras

        if num_cameras == 1:
            camera_id = 0
            logging.info('Found one camera: %s.' % cameras_found[0])
        else:
            logging.info('Found %d cameras.' % num_cameras)
            for n in range(num_cameras):
                logging.info('    %d: %s' % (n, cameras_found[n]))
            # TO DO: allow user to select a camera
            camera_id = 0
            logging.info('Using #%d: %s.' % (camera_id, cameras_found[camera_id]))

        self.camera = asi.Camera(camera_id)
        self.camera_info = self.camera.get_camera_property()

        # Get all of the camera controls
        self.controls = self.camera.get_controls()

        # Use minimum USB bandwidth permitted
        self.camera.set_control_value(asi.ASI_BANDWIDTHOVERLOAD, self.camera.get_controls()['BandWidth']['MinValue'])

        # Set some sensible defaults. They will need adjusting depending upon
        # the sensitivity, lens and lighting conditions used.
        self.camera.disable_dark_subtract()

        self.camera.set_control_value(asi.ASI_GAIN, self.gain)
        self.camera.set_control_value(asi.ASI_EXPOSURE, self.exposure)
        self.camera.set_control_value(asi.ASI_WB_B, 99)
        self.camera.set_control_value(asi.ASI_WB_R, 75)
        self.camera.set_control_value(asi.ASI_GAMMA, 50)
        self.camera.set_control_value(asi.ASI_BRIGHTNESS, 50)
        self.camera.set_control_value(asi.ASI_FLIP, 0)

        logging.info('Enabling stills mode')
        self.camera.stop_video_capture()
        self.camera.stop_exposure()

        logging.info('Starting capture thread')
        self.thread = threading.Thread(target=self.capture_task)
        self.thread.daemon = True
        self.thread.start()

    def capture_task(self):
        while True:
            #if self.lock.acquire(blocking=True, timeout=TIMEOUT):
            if self.lock.acquire(blocking=False):
                try:
                    self.camera.set_image_type(asi.ASI_IMG_RAW8)
                    self.img = self.camera.capture()
                    self.error_count = 0
                except asi.ZWO_CaptureError:
                    logging.warning('Capture error')
                    image = Image.new('RGB',IMAGE_SIZE,'black')
                    draw = ImageDraw.Draw(image)
                    font = ImageFont.truetype(FONT, size=100)
                    draw.text((10, 10), "Busy...", fill='red', font=font)
                    self.img = np.array(image)
                    self.error_count += 1
                    if self.error_count > MAX_ERROR:
                        logging.critical("That's all for me, I'm outta here!")
                        pid = os.getpid()
                        os.kill(pid, signal.SIGTERM)
                finally:
                    self.lock.release()

            time.sleep(0.2)

    def get_image(self):
        if self.lock.acquire(blocking=True, timeout=TIMEOUT):
            image = Image.fromarray(self.img)
            #enh = ImageEnhance.Brightness(image)
            #image = enh.enhance(10)
            draw = ImageDraw.Draw(image)
            font = ImageFont.truetype(FONT, size=50)
            width, height = image.size
            text = time.strftime("%c")
            x = 100
            y = height - 100
            b = 15
            (x1,y1,x2,y2) = font.getbbox(text)
            draw.rectangle((x+x1-b,y+y1-b,x+x2+b,y+y2+b), fill='black')
            draw.text((x, y), text, fill='white', font=font)
            self.lock.release()        
        else:
            logging.warning(f'Could not aquire lock within {TIMEOUT}s')
            image = Image.new('RGB',IMAGE_SIZE,'black')
            draw = ImageDraw.Draw(image)
            font = ImageFont.truetype(FONT, size=100)
            draw.text((10, 10), "Could not acquire lock", fill='red', font=font)
        return image

    def set_control(self, control, value):
        logging.info(f'Setting control {control} to {value}')
        if control in [ asi.ASI_GAIN, \
                        asi.ASI_EXPOSURE, \
                        asi.ASI_GAMMA, \
                        asi.ASI_BRIGHTNESS] and value != None:
            self.camera.stop_exposure()
            self.camera.set_control_value(control, value)
            return True
    
    def get_controls(self):
        return self.camera.get_control_values()

    def reset(self):
        logging.info('Reset')
        self.camera.stop_exposure()
        self.gain = DEFAULT_GAIN
        self.exposure = DEFAULT_EXPOSURE
        self.brightness = DEFAULT_BRIGHTNESS
        self.gamma = DEFAULT_GAMMA
        self.camera.set_control_value(asi.ASI_GAIN, self.gain)
        self.camera.set_control_value(asi.ASI_EXPOSURE, self.exposure)
        self.camera.set_control_value(asi.ASI_GAMMA, self.gamma)
        self.camera.set_control_value(asi.ASI_BRIGHTNESS, self.brightness)


def get_ip():
    st = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        st.connect(('8.8.8.8', 1))
        ip = st.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        st.close()
    return ip

@app.route('/set_gamma')
def set_gamma():
    value = request.args.get('value', default=DEFAULT_GAMMA, type=int)
    if value >=0 and value <= 100:
        if skycam.set_control(asi.ASI_GAMMA, value):
            return jsonify({'success': True})
    return jsonify({'success': False})

@app.route('/set_brightness')
def set_brightness():
    value = request.args.get('value', default=DEFAULT_BRIGHTNESS, type=int)
    if value >=0 and value <= 100:
        if skycam.set_control(asi.ASI_BRIGHTNESS, value):
            return jsonify({'success': True})
    return jsonify({'success': False})

@app.route('/set_gain')
def set_gain():
    value = request.args.get('value', default=DEFAULT_GAIN, type=int)
    if value >=0 and value <= 510:
        if skycam.set_control(asi.ASI_GAIN, value):
            return jsonify({'success': True})
    return jsonify({'success': False})

@app.route('/set_exposure')
def set_exposure():
    value = request.args.get('value', default=DEFAULT_EXPOSURE, type=int)
    if value >=0 and value <= 10000000:
        if skycam.set_control(asi.ASI_EXPOSURE, value):
            return jsonify({'success': True})
    return jsonify({'success': False})        

@app.route('/get_controls')
def get_controls():
    values = skycam.get_controls()
    return jsonify(values)

@app.route('/get_camera_info')
def get_camera_info():
    return jsonify(skycam.camera_info)


@app.route('/get_camera_controls')
def get_camera_controls():
    return jsonify(skycam.controls)
    
@app.route('/get_image')
def get_image():
    size = request.args.get('size', type=int)
    image = skycam.get_image()
    if size and size>=0 and size<=IMAGE_SIZE[0]:
        image.thumbnail((size, size), resample=Image.Resampling.LANCZOS)
    out = io.BytesIO()
    image.save(out, format='jpeg')
    out.seek(0)
    return send_file(out, mimetype='image/jpeg')

@app.route('/reset')
def reset():
    skycam.reset()
    return jsonify({'success': True}) 


if __name__ == '__main__':
    format = '%(asctime)s - %(levelname)s - %(message)s'
    loglevel = logging.WARNING

    addr = get_ip()
    port = DEFAULT_PORT
    
    env_filename = os.getenv('ZWO_ASI_LIB')

    parser = argparse.ArgumentParser(description='Skycam Capture Process')
    parser.add_argument('--sdk',  nargs='?', metavar='FILENAME', help='ZWO SDK library filename')
    parser.add_argument('--log',  nargs='?', metavar='INFO|WARNING|CRITICAL', default='WARNING', help='Loglevel (default: %(default)s)')
    parser.add_argument('--bind', nargs='?', metavar='ADDRESS', help='Bind to address')
    parser.add_argument('--port', nargs='?', type=int, default=5000, help=f'Listening port (default: %(default)s)')
    args = parser.parse_args()

    if args.log:
        loglevel = getattr(logging, args.log.upper())
        if not isinstance(loglevel, int):
            raise ValueError(f'Ivalid log level: {loglevel}')
    
    logging.basicConfig(format=format, level=loglevel)

    if args.sdk:
        sdk = args.sdk
    elif env_filename:
        sdk = env_filename
    else:
        logging.critical('The filename of the SDK library is required (or set ZWO_ASI_LIB\
                          environment variable with the filename).')
        sys.exit(1)

    if args.bind:        
        try:
            socket.inet_aton(args.bind)
            addr = args.bind
        except socket.error:
            logging.warning(f'Invalid address {args.bind}. Using {addr} instead')
    
    if args.port:
        if args.port >= 1023 and args.port <= 65535: 
            port = args.port
        else:
            logging.warning(f'Invalid port {args.port}. Using default port {DEFAULT_PORT} instead')

    try:
        skycam = Skycam(sdk)
    except RuntimeError as e:
        logging.critical(e)
        sys.exit(1)

    
    logging.info(f'Starting server on {addr}:{port}')
    server = create_server(app, host=addr, port=port, threads=10)
    server.run()
