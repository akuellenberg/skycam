import React, {useState} from 'react'
import Cam from './Cam'
import './skycam.css'

export default function App(props) {
    return (
        <div className="App">
            <header>
                <h1>Skycam</h1>
            </header>
            <main>
                <Cam url={props.url} />
            </main>
            <footer>
                <div className='div--support'>
                    Support: <a href="mailto:akuellenberg@mpifr-bonn.mpg.de" target="_new">André Küllenberg</a>
                </div>
            </footer>
        </div>
    )
}