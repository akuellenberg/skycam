import React, { useState, useEffect } from 'react'
import Controls from './Controls'

export default function Cam(props) {
    const [error, setError] = useState(true)
    const [timerId, setTimerId] = useState() 
    const [image, setImage] = useState()

    useEffect(() => {
        setTimerId(setInterval(
            () => tick(),
            1000
        ))
        return (() => clearInterval(timerId))
    },[])

    function tick() {
        const date = new Date()
        let size = 0
        if (typeof camImg != 'undefined') {
            size = camImg.width
        }
        const src = props.url + '/get_image?size=' + size + '&' + date.getTime()
        URL.revokeObjectURL(image)
        fetch(src)
            .then((res) => res.blob())
            .then((imageBlob) => {
                setImage(URL.createObjectURL(imageBlob))
                setError(false)
            })
            .catch((error) => {
                setError(true)
            })
    }

    if (!error) {
        return (
            <div>
                <div className="div--cam">
                    <img name="camImg" src={image} alt="Skycam" />
                </div>
                <Controls url={props.url} />
            </div>
        )
    } else {
        return (
            <div className="div--error">
                <h3>No response from {props.url} - Check if capture process is running.</h3>
            </div>
        )
    }
}