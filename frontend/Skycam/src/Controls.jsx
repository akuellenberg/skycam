import React, {useState, useEffect} from 'react'

export default function Controls(props) {
    const [control, setControl] = useState({'gain': 0, 'exposure': 0})
    const [reset, setReset] = useState(false)
    
    useEffect(() => {
        fetch(props.url + '/get_controls')
        .then(resp => resp.json())
        .then(data => setControl({'gain': data.Gain, 'exposure': data.Exposure / 1000}))
        setReset(false)
    },[reset])

    function changeHandler(event) {
        const {name, value} = event.target
        let newVal = value
        if (name === 'exposure') {
            newVal *= 500;
        }
        fetch(`${props.url}/set_${name}?value=${newVal}`)
        .then(resp => resp.json())
        .then(data => {data.success ? setControl({...control, [name]: value}) : console.log("fail")})        
    }

    function useDefaultsHandler() {
        fetch(`${props.url}/reset`)
        .then(resp => resp.json())
        .then(data => {data.success ? setReset(true) : console.log("fail")}) 
    }

    return (
        <div className='div--controls'>
            <div className='div--sliders'>
                <label htmlFor="gain">Camera gain: {control.gain}</label>
                <input name="gain" type="range" min="0" max="510" value={control.gain} onInput={changeHandler}/>
                <label htmlFor="exposure">Exposure time: {control.exposure}ms</label>
                <input name="exposure" type="range" min="1" max="5000" value={control.exposure} onInput={changeHandler}/>
                {/* <label htmlFor="gamma">Gamma: {control.gamma}</label>
                <input name="gamma" type="range" min="1" max="100" value={control.gamma} onInput={changeHandler}/>
                <label htmlFor="brightness">Brightness: {control.brightness}</label>
                <input name="brightness" type="range" min="1" max="100" value={control.brightness} onInput={changeHandler}/> */}
            </div>
            <div className='div--buttons'>
                <button type="button" onClick={useDefaultsHandler}>Use default values</button>
                {/* <button type="button" onClick={resetHandler}>Reset</button> */}
            </div>
        </div>
    );
}