import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
 
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App url={`http://${window.location.hostname}:5000`} />
  </React.StrictMode>
)
{/* <App url={import.meta.env.VITE_CAPTURE_URL} /> */}