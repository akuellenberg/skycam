# Skycam 

Work in progress...
## Hardware

* Raspberry Pi 4b
  * Ethernet MAC Address: ```e4:5f:01:ab:de:df```
  * RAM: 4GB
  * SD Karte: Sandisks Extreme 32GB
* Kamera: 
  * ZWO ASI 178 MM Mono
  * Sensor: CMOS Sony IMX178
  * Auflösung: 3096 x 2080
  * Bit-Tiefe: 14
  * Chip-Größe: 7,37 x 4,92mm
  * Anschluß: T2
* Objektiv:
  * Canon EF 50mm f/1.8 STM
* Adapter:
  * ZWO EOS-T2 Adapter

### Raspberry Pi
#### Finden der IP-Adresse des Raspberry Pis im Netz:

	sudo nmap -sP 134.104.73.0/24  | awk '/^Nmap/{ip=$NF}/E4:5F:01:AB:DE:DF/{print ip}'

#### Shellzugang per ssh

	ssh skycam@<IP-Adresse>

### Objektiv
Der Autofokus des Objektivs kann an der Kamera nicht verwendet werden. Der bewegliche Frontteil des Objektivs muss manuell komplett nach innen geschoben werden. In Verbindung mit dem ZWO EOS-Adapter ist ein zusätzlicher 1.3mm Abstandsring erfordlich, um eine scharfe Abbildung im unendlichen zu erreichen.
Mit der Kombination aus Kamera und Objektiv ergibt sich ein Blickwinkel von ca. 8x5°:
![Annotated skyview](annotated.jpeg)

## Software

Die Software besteht aus zwei Komponenten: 

1. Capture Prozess
2. Web-Frontend

### Start der Software

Die Software läuft im Docker-Container. Der Aufruf erfolgt vorzugsweise per docker-compose:

	skycam@Skycam:~ $ docker-compose up -d

Dabei werden sowohl der Capture Prozess als auch der Webserver gestartet. Sofern die Images noch nicht erstellt wurden, erfolgt der Build-Prozess automatisch. Der Build nimmt auf dem Raspberry Pi einige Zeit in Anspruch.

### Capture Prozess

Das Capture Programm verwendet ein Python Binding für die ZWO C Library.

* ZWO SDK: https://astronomy-imaging-camera.com/software-drivers
* Python Binding: https://github.com/python-zwoasi/python-zwoasi

#### Aufruf

	capture.py [-h] [--sdk [FILENAME]] [--log [INFO|WARNING|CRITICAL]]
					[--bind [ADDRESS]] [--port [PORT]]

	optional arguments:
	-h, --help            show this help message and exit
	--sdk [FILENAME]      ZWO SDK library filename
	--log [INFO|WARNING|CRITICAL]
							Loglevel (default: WARNING)
	--bind [ADDRESS]      Bind to address
	--port [PORT]         Listening port (default: 5000)

Die Angabe der SDK Library ist zwingend erforderlich und erfolgt entweder über den Komandozeilenparameter oder per Umgebungsvariable: 

	skycam@Skycam:~ $ export ZWO_ASI_LIB=/usr/local/lib/libASICamera2.so

#### REST API

Der Capture Prozess stellt eine REST API auf Port 5000 zur Verfügung und kennt die folgenden Befehle:

* ```/get_image?size=[pixel]```

   Sendet das Kamerabild als mimetype "image/jpeg", skaliert auf die Breite *size*. Die Angabe von *size* ist optional.

* ```/get_controls```

   Sendet ein JSON Objekt mit den aktuellen Kameraeinstellungen

* ```/reset```

   Setzt die Kameraeinstellungen auf Default zurück.

* ```/set_exposure?value=[exposure time in µs]```

   Einstellung der Belichtungszeit (1...10000000µs)

* ```/set_gain?value=[gain]```

   Einstellung der Verstärkung (0...510)

#### Docker Container

Nur für Tests und als Gedächtnisstütze :-)

Docker Container für den Capture Prozess erstellen

	docker build -t mpifr/capture:0.1 .

Alternativ mit Qemu und buildx

	docker buildx create --use
	docker buildx build --platform linux/arm -t mpifr/capture:0.1 .

Intermediate images löschen

	docker rmi -f $(docker images -q --filter "dangling=true")

Docker container ausführen

	docker run -p 5000:5000 --privileged --rm -ti mpifr/capture:0.1 /bin/bash

Capture Prozess manuell starten

	/usr/src/capture.py --sdk /usr/local/lib/libASICamera2.so --log DEBUG

Test im Browser

	http://<hostname>:5000/get_image

### Web-Frontend

Das Web-Frontend basiert auf ReactJS und Vite. Nach dem Start des Docker-Stacks ist das Frontend im Browser aufrufbar:

	http://<hostname>/

![Screenshot](frontend.png)

Nach Bearbeitung der Webseiten müssen diese manuell in das Docker-Verzeichnis kopiert werden.

Start der Entwicklungsumgebung:

	npm run dev

Zu Testzwecken steht dann ein Webserver auf dem Port 5173 zur Verfügung.

Build Prozess:

	npm run build

Die exportierten Webseiten und Javascript Dateien können anschließend in den Docker-Container kopiert werden:
	
	skycam@Skycam:~ $ cp -r frontend/Skycam/dist/* docker/skycam/www/dist

Anschließend muss der Container für das Web-Frontend neu erstellt werden:

	skycam@Skycam:~/docker/skycam $ docker-compose build --no-cache skycam


